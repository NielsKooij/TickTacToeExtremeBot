package com.nielskooij.tictactoeextreme.board;

import static org.junit.Assert.assertEquals;

import org.junit.Before;

import com.nielskooij.tictactoeextreme.Move;
import com.nielskooij.tictactoeextreme.board.Board;

public class BasicBoardTestSetup {

	protected Board board;
	
	@Before
	public void setUp() throws Exception {
		board = new Board();
		
		doValidatedMove(new Move(2, 4), true);//1
		doValidatedMove(new Move(7, 3), true);//2
		doValidatedMove(new Move(4, 1), true);//1
		doValidatedMove(new Move(4, 5), true);//2
		doValidatedMove(new Move(4, 6), true);//1
		doValidatedMove(new Move(3, 0), true);//2
		doValidatedMove(new Move(0, 0), true);//1
		doValidatedMove(new Move(1, 1), true);//2
		doValidatedMove(new Move(3, 4), true);//1
		doValidatedMove(new Move(2, 5), true);//2
		doValidatedMove(new Move(6, 7), true);//1
		doValidatedMove(new Move(1, 4), true);//2
		doValidatedMove(new Move(5, 3), true);//1
		doValidatedMove(new Move(7, 1), true);//2
		doValidatedMove(new Move(5, 4), true);//1
		doValidatedMove(new Move(7, 4), true);//2
		
		/*
		 * 1 0 0	2 0 0	0 0 0
		 * 0 2 0	0 1 0	0 2 0
		 * 0 0 0	0 0 0	0 0 0
		 * 
		 * 0 0 0	0 0 1	0 2 0
		 * 0 2 1	1 0 1	0 2 0
		 * 0 0 2	0 2 0	0 0 0
		 * 
		 * 0 0 0	0 1 0	0 0 0
		 * 0 0 0	0 0 0	1 0 0
		 * 0 0 0	0 0 0	0 0 0
		 * 
		 * Player 1 has to move in the middle.
		 */
	}
	
	protected void doValidatedMove(Move m, boolean expected) {
		assertEquals(expected, board.isValidMove(m));
		board.doMove(m);
	}
	
}
