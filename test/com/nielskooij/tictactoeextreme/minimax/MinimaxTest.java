package com.nielskooij.tictactoeextreme.minimax;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.nielskooij.tictactoeextreme.Move;
import com.nielskooij.tictactoeextreme.board.Board;
import com.nielskooij.tictactoeextreme.board.BoardParser;
import com.nielskooij.tictactoeextreme.board.score.SimpleScore;

public class MinimaxTest {

	private Minimax minimax;
	private Board board;
	
	@Before
	public void setUp() throws Exception {
		minimax = new Minimax(new SimpleScore(), 2);
		
		board = new Board();
		board.setMyId(2);
	}

	@Test
	public void canFindFinalMove() {
		BoardParser.parseMicroBoard("0,1,0,1,.,.,.,1,.,"
								  + ".,1,.,.,0,.,0,1,.,"
								  + ".,1,.,0,0,.,.,.,.,"
								  + "1,.,.,.,.,0,.,1,.,"
								  + ".,1,0,0,.,0,.,1,.,"
								  + ".,.,1,.,1,0,.,1,.,"
								  + ".,.,1,.,0,.,0,.,.,"
								  + ".,.,.,0,.,0,0,.,.,"
								  + ".,.,1,.,1,.,0,.,1", 
								  board);
		BoardParser.parseMacroBoard("1,.,.,1,0,1,.,.,0", board);
		board.doMove(new Move(4, 7));
		
		minimax.calculateBestMove(Integer.MIN_VALUE, Integer.MAX_VALUE, board, 0);
		Move bestMove = minimax.getBestMove();
		
		assertEquals(2, bestMove.getMicroX());
		assertEquals(7, bestMove.getMicroY());
	}
	
	@Test
	public void canFindFinalFewMoves() {
		BoardParser.parseMicroBoard("0,1,0,1,.,.,.,1,.,"
								  + ".,1,.,.,0,.,0,1,.,"
								  + ".,1,.,0,.,0,.,.,.,"
								  + "1,.,.,.,.,0,.,1,.,"
								  + ".,1,0,0,.,0,.,1,.,"
								  + ".,.,1,.,1,0,.,.,.,"
								  + ".,.,1,.,0,.,0,.,.,"
								  + ".,.,.,0,.,.,0,.,.,"
								  + ".,.,1,.,1,.,0,.,1", 
								  board);
		BoardParser.parseMacroBoard("1,.,.,1,0,.,.,.,0", board);
		board.doMove(new Move(5, 7));
		
		minimax.calculateBestMove(Integer.MIN_VALUE, Integer.MAX_VALUE, board, 0);
		Move bestMove = minimax.getBestMove();
		
		assertEquals(7, bestMove.getMicroX());
		assertEquals(5, bestMove.getMicroY());
	}
	
}
