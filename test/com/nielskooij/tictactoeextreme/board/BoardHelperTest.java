package com.nielskooij.tictactoeextreme.board;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.nielskooij.tictactoeextreme.MatrixTestUtil;
import com.nielskooij.tictactoeextreme.Move;
import com.nielskooij.tictactoeextreme.board.BoardHelper;

public class BoardHelperTest extends BasicBoardTestSetup {

	@Test
	public void getAvailableMovesFullMacroCell(){
		board.doMove(new Move(5, 5));
		board.doMove(new Move(7, 7));
		
		List<Move> availablesMoves = BoardHelper.getAvailableMoves(board);
		
		assertEquals(59, availablesMoves.size());
	}
	
	@Test
	public void getAvailableMovesTest() {
		assertEquals(5, BoardHelper.getAvailableMoves(board).size());
	}
	
	@Test
	public void whoWonEmpty() {
		int[][] cell = MatrixTestUtil.buildMatrix(0, 0, 0, 
												  0, 0, 0, 
												  0, 0, 0);
		
		assertEquals(0, BoardHelper.getWinner(cell));
	}
	
	@Test
	public void whoWonHorizontal() {
		int[][] cell = MatrixTestUtil.buildMatrix(1, 1, 1, 
											 	  0, 0, 0, 
											 	  0, 0, 0);
		
		assertEquals(1, BoardHelper.getWinner(cell));
	}
	
	@Test
	public void whoWonVertical() {
		int[][] cell = MatrixTestUtil.buildMatrix(0, 2, 0, 
												  0, 2, 0, 
												  0, 2, 0);
		
		assertEquals(2, BoardHelper.getWinner(cell));
	}
	
	@Test
	public void whoWonDiagonal() {
		int[][] cell = MatrixTestUtil.buildMatrix(2, 0, 0, 
												  0, 2, 0, 
												  0, 0, 2);
		
		assertEquals(2, BoardHelper.getWinner(cell));
	}
	
	@Test
	public void whoWonOtherDiagonal() {
		int[][] cell = MatrixTestUtil.buildMatrix(0, 0, 1, 
												  0, 1, 0, 
												  1, 0, 0);
		
		assertEquals(1, BoardHelper.getWinner(cell));
	}

	@Test
	public void whoWonTie() {
		int[][] cell = MatrixTestUtil.buildMatrix(1, 2, 1, 
												  1, 2, 1, 
												  2, 1, 2);
		
		assertEquals(3, BoardHelper.getWinner(cell));
	}
	
	@Test
	public void macroCellMatrixTest() {
		int[][] cell = MatrixTestUtil.buildMatrix(0,0,1,
												  1,0,1,
												  0,2,0);
		
		MatrixTestUtil.intDoubleArrayEquals(cell, BoardHelper.getMacroCellMatrix(board, 1, 1), 3, 3);
	}
	
}
