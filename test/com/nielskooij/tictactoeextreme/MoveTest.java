package com.nielskooij.tictactoeextreme;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.nielskooij.tictactoeextreme.Move;

public class MoveTest {

	private Move m;
	
	@Before
	public void setUp() throws Exception {
		m = new Move(2, 5);
	}

	@Test
	public void deepCopyTest() {
		Move copy = m.deepCopy();
		
		assertEquals(false, copy.equals(m));
		assertEquals(copy.getMacroX(), m.getMacroX());
		assertEquals(copy.getMacroY(), m.getMacroY());
		assertEquals(copy.getMicroX(), m.getMicroX());
		assertEquals(copy.getMicroY(), m.getMicroY());
	}

}
