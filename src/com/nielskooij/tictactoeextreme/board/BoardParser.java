package com.nielskooij.tictactoeextreme.board;

public class BoardParser {
	
	public static void parseMicroBoard(String boardString, Board board) {
		int[][] microBoard = board.getMicroBoard();
		
		int counter = 0;
		String[] r = boardString.split(",");
		for (int y = 0; y < Constants.MICRO_CELL_DIM; y++) {
			for (int x = 0; x < Constants.MICRO_CELL_DIM; x++) {
				try {
					microBoard[x][y] = Integer.parseInt(r[counter]) + 1; 
				}catch(NumberFormatException e) {
					microBoard[x][y] = Constants.EMPTY_CELL;
				}
				counter++;
			}
		}
	}
	
	public static String microBoardAsString(Board board) {
		int[][] microBoard = board.getMicroBoard();
		String result = "";
		
		for(int y = 0; y < Constants.MICRO_CELL_DIM; y++) {
			for(int x = 0; x < Constants.MICRO_CELL_DIM; x++) {
				result += microBoard[x][y] + ",";
			}
		}
		
		result = result.substring(0, result.length() - 1)
				.replaceAll("0", ".");
		
		return result;
	}
	
	public static void parseMacroBoard(String boardString, Board board) {
		int[][] macroBoard = board.getMacroBoard();
		
		int counter = 0;
		String[] r = boardString.split(",");
		for (int y = 0; y < Constants.MACRO_CELL_DIM; y++) {
			for (int x = 0; x < Constants.MACRO_CELL_DIM; x++) {
				try {
					int value = Integer.parseInt(r[counter]);
					macroBoard[x][y] = value + ((value != -1) ? 1 : 0);
				}catch(NumberFormatException e) {
					macroBoard[x][y] = Constants.EMPTY_CELL;
				}
				counter++;
			}
		}
	}
	
	public static String macroBoardAsString(Board board) {
		int[][] macroBoard = board.getMacroBoard();
		String result = "";
		
		for(int y = 0; y < Constants.MACRO_CELL_DIM; y++) {
			for(int x = 0; x < Constants.MACRO_CELL_DIM; x++) {
				result += macroBoard[x][y] + ",";
			}
		}
		
		result = result.substring(0, result.length() - 1)
				.replaceAll("0", ".");
		
		return result;
	}
	
}
