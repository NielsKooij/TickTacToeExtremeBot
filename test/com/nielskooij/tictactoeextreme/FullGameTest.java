package com.nielskooij.tictactoeextreme;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.nielskooij.tictactoeextreme.board.Board;
import com.nielskooij.tictactoeextreme.board.BoardCopyUtil;
import com.nielskooij.tictactoeextreme.board.BoardHelper;
import com.nielskooij.tictactoeextreme.board.BoardParser;
import com.nielskooij.tictactoeextreme.board.score.SimpleScore;
import com.nielskooij.tictactoeextreme.bot.BotMinimax;
import com.nielskooij.tictactoeextreme.bot.BotRandom;
import com.nielskooij.tictactoeextreme.bot.IBot;

public class FullGameTest {

	private IBot randomBot;
	private IBot minimaxBot;
	
	private Board board;
	
	@Before
	public void setup() {
		randomBot = new BotRandom();
		minimaxBot = new BotMinimax(new SimpleScore(), 5);
	
		board = new Board();
	}
	
	@Test
	public void minimaxFirst() {
		board.setMyId(1);
		board.setOpponentId(2);
		
		playGame();
	}
	
	@Test
	public void minimaxSecond() {
		board.setMyId(2);
		board.setOpponentId(1);
		
		playGame();
	}

	private void playGame() {
		while(!BoardHelper.isGameOver(board)) {
			if(board.getTurn() == board.getMyId()) {
				doBestMove(minimaxBot);
			}else {
				doBestMove(randomBot);
			}
		}
	}
	
	private void doBestMove(IBot bot) {
		Board copy = BoardCopyUtil.copy(BoardParser.microBoardAsString(board), BoardParser.macroBoardAsString(board), board.getMyId(), board.getOpponentId(), board.getTurn());
		
		Move move = bot.getBestMove(copy);
		assertTrue(board.isValidMove(move));
		board.doMove(move);
	}
	
}
