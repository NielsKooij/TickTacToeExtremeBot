package com.nielskooij.tictactoeextreme.bot;

import java.util.List;
import java.util.Random;

import com.nielskooij.tictactoeextreme.Move;
import com.nielskooij.tictactoeextreme.board.Board;
import com.nielskooij.tictactoeextreme.board.BoardHelper;
import com.nielskooij.tictactoeextreme.board.score.IBoardScore;
import com.nielskooij.tictactoeextreme.minimax.Minimax;

public class BotMinimax implements IBot {

	private Minimax minimax;
	
	public BotMinimax(IBoardScore scoring, int maxDepth) {
		minimax = new Minimax(scoring, maxDepth);
	}

	public Move getBestMove(Board board) {
		if(board.getMoves().isEmpty()){
			List<Move> availableMoves = BoardHelper.getAvailableMoves(board);
			
			Random rand = new Random();
			int index = rand.nextInt(availableMoves.size());
			
			return availableMoves.get(index);
		}
		
		minimax.calculateBestMove(Integer.MIN_VALUE, Integer.MAX_VALUE, board, 0);
		return minimax.getBestMove();
	}

}
