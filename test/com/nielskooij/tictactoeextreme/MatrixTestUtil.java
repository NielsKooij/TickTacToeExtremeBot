package com.nielskooij.tictactoeextreme;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MatrixTestUtil {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		int[][] matrix = buildMatrix(1, 2, 3, 
									 4, 5, 6, 
									 7, 8, 9);
		
		int[][] expected = new int[][]{
			new int[]{1, 4, 7},
			new int[]{2, 5, 8},
			new int[]{3, 6, 9}
		};
		
		intDoubleArrayEquals(matrix, expected, 3, 3);
	}
	
	public static int[][] buildMatrix(int... values){
		int dim = (int) Math.sqrt(values.length);
		
		int[][] result = new int[dim][dim];
		
		for(int i = 0; i < values.length; i++){
			int x = i % dim;
			int y = i / dim;
			
			result[x][y] = values[i];
		}
		
		return result;
	}
	
	public static void intDoubleArrayEquals(int[][] arr1, int[][] arr2, int dimX, int dimY){
		for (int x = 0; x < dimX; x++) {
			for (int y = 0; y < dimY; y++) {
				assertEquals(x + ", " + y, arr1[x][y], arr2[x][y]);
			}
		}
	}

}
