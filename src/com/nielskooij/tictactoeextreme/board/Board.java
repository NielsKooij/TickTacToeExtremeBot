package com.nielskooij.tictactoeextreme.board;

import java.util.Stack;

import com.nielskooij.tictactoeextreme.Move;

public class Board {

	private int myId;
	private int opponentId;
	
	private int[][] microBoard;
	private int[][] macroBoard;
	
	private int turn = 1;
	
	private Stack<Move> moves;
	
	public Board() {
		resetBoard();
		
		for(int y = 0; y < Constants.MACRO_CELL_DIM; y++) {
			for(int x = 0; x < Constants.MACRO_CELL_DIM; x++) {
				macroBoard[x][y] = -1;
			}
		}
	}
	
	private void resetBoard() {
		microBoard = new int[Constants.MICRO_CELL_DIM][Constants.MICRO_CELL_DIM];
		macroBoard = new int[Constants.MACRO_CELL_DIM][Constants.MACRO_CELL_DIM];
		moves = new Stack<>();
	}
	
	/**
	 * Parse the state from a string.
	 * @param microBoard The microBoard as string.
	 * @param macroBoard The macroBoard as string.
	 */
	public void parseFromString(String microBoard, String macroBoard) {
		resetBoard();
		BoardParser.parseMicroBoard(microBoard, this);
		BoardParser.parseMacroBoard(macroBoard, this);
	}
	
	/**
	 * Check if a move is valid.
	 * @param move The move.
	 * @return true if the move is valid.
	 */
	public boolean isValidMove(Move move) {
		return macroBoard[move.getMacroX()][move.getMacroY()] == Constants.PLACABLE_CELL && 
			   microBoard[move.getMicroX()][move.getMicroY()] == Constants.EMPTY_CELL;
	}
	
	/**
	 * Do a move on the board.
	 * @param move The move.
	 */
	public void doMove(Move move) {
		microBoard[move.getMicroX()][move.getMicroY()] = turn;
		moves.push(move);
		updateMacroBoard(move);
		
		nextTurn();
	}
	
	private void nextTurn() {
		if(turn == 1) {
			turn = 2;
		}else {
			turn = 1;
		}
	}
	
	/**
	 * Undo the last move.
	 * @return The last move or null if there are no moves.
	 */
	public Move undoLastMove() {
		Move m = null;
		if(!moves.isEmpty()) {
			m = moves.pop();
			microBoard[m.getMicroX()][m.getMicroY()] = 0;
			nextTurn();
			
			undoLastMoveMacroBoard(m);
		}

		return m;
	}
	
	private void undoLastMoveMacroBoard(Move lastMove) {
		int macroValue = macroBoard[lastMove.getMacroX()][lastMove.getMacroY()];
		int updateValue = (macroValue < 0) ? -1 : 0;
		
		for(int y = 0; y < Constants.MACRO_CELL_DIM; y++) {
			for(int x = 0; x < Constants.MACRO_CELL_DIM; x++) {			
				if(macroBoard[x][y] < 0) {
					macroBoard[x][y] = updateValue;
				}
			}
		}
		
		if(macroValue >= 0) {
			macroBoard[lastMove.getMacroX()][lastMove.getMacroY()] = -1;
		}
		
	}
	
	public int getTurn() {
		return turn;
	}

	public int[][] getMicroBoard() {
		return microBoard;
	}

	public int[][] getMacroBoard() {
		return macroBoard;
	}
	
	public int getMyId() {
		return myId;
	}

	public void setMyId(int myId) {
		this.myId = myId;
	}

	public int getOpponentId() {
		return opponentId;
	}

	public void setOpponentId(int opponentId) {
		this.opponentId = opponentId;
	}
	
	/**
	 * This method is mainly for testing.
	 * @return
	 */
	public Stack<Move> getMoves(){
		return moves;
	}
	
	/**
	 * This method is mainly for testing.
	 * @return
	 */
	public Move getLastMove() {
		return moves.peek();
	}
	
	public void updateMacroBoard(Move move) {
		int[][] subCell = BoardHelper.getMacroCellMatrix(this, move.getMacroX(), move.getMacroY()); 
		macroBoard[move.getMacroX()][move.getMacroY()] = BoardHelper.getWinner(subCell);
		
		int macroValue = macroBoard[move.getNextMoveMacroX()][move.getNextMoveMacroY()];
		int updateValue = (macroValue <= 0) ? 0 : -1;
		
		for(int y = 0; y < Constants.MACRO_CELL_DIM; y++) {
			for(int x = 0; x < Constants.MACRO_CELL_DIM; x++) {
				if(macroBoard[x][y] <= 0) {
					macroBoard[x][y] = updateValue;
				}
			}
		}
		
		if(macroValue <= 0) macroBoard[move.getNextMoveMacroX()][move.getNextMoveMacroY()] = -1;
	}

	public void setTurn(int turn) {
		this.turn = turn;
	}
	
}
