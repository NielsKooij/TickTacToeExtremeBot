package com.nielskooij.tictactoeextreme.board;

public class BoardCopyUtil {

	public static Board copy(String microBoard, String macroBoard, int myId, int opponentId, int turn) {
		Board copy = new Board();
		BoardParser.parseMicroBoard(microBoard, copy);
		BoardParser.parseMacroBoard(macroBoard, copy);
		
		copy.setMyId(myId);
		copy.setOpponentId(opponentId);
		copy.setTurn(turn);
		
		return copy;
	}
	
}
